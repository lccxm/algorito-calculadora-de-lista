public class Pilha {

    private class Node {
        public Double element;
        public Node next;
        public Node prev;

        public Node(Double e) {
            element = e;
            next = null;
            prev = null;
        }
    }

    private Node header;
    private Node trailer;
    private int count;
    private Double x;
    private Double y;

    public Pilha() {
        header = new Node(null);
        trailer = new Node(null);
        header.next = trailer;
        trailer.prev = header;
        count = 0;
    }


    public void push(Double element) {
        Node n = new Node(element);
        Node last = trailer.prev;
        n.prev = last;
        n.next = trailer;
        last.next = n;
        trailer.prev = n;
        count++;
    }

    public Double pop() {
        Node n = trailer.prev;
        if (n.element != null) {
            x = n.element;
            n.prev.next = n.next;
            n.next.prev = n.prev;
            count--;
        }
        return x;
    }

    public Double top() {
        Node n = trailer.prev;
        return n.element;
    }

    public int size() {
        return count;
    }

    public boolean isEmpty() {
        return (count == 0);
    }

    public void clear() {
        header = new Node(null);
        trailer = new Node(null);
        header.next = trailer;
        trailer.prev = header;
        count = 0;
    }

    public void dup() {
        Node ant = trailer.prev;
        Node n = new Node(ant.element);
        ant.next = n;
        n.prev = ant;
        n.next = trailer;
        count++;
    }

    public void swap() {
        if (count >= 2) {
            Node a = trailer.prev;
            Node b = a.prev;
            Double aux = a.element;
            a.element = b.element;
            b.element = aux;
        }
    }

    public void chs() {
        Node n = trailer.prev;
        n.element = n.element * -1;
    }

    public void sqrt() {
            Node n = trailer.prev;
            n.element = Math.sqrt(n.element);
    }

}
