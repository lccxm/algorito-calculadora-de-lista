import java.io.*;

public class Calculadora {

    private Double result;
    private boolean existe;
    private Pilha pilha;
    private String linha;
    private Double x;
    private Double y;
    private int tamMax;
    private Double num;



    public Calculadora(Pilha p) {
        pilha = p;

        tamMax = 0;
    }


    public void le() throws IOException {
        File arquivo = new File("exemplo2.txt");
        FileReader fr = new FileReader(arquivo);
        BufferedReader br = new BufferedReader(fr);
        while ((linha = br.readLine()) != null) {
            if (linha.equals("+") || linha.equals("-") || linha.equals("*") || linha.equals("/") || linha.equals("swap") || linha.equals("sqrt") || linha.equals("dup") || linha.equals("pop") || linha.equals("chs")) {
                if (linha.equals("+")) {
                    if (pilha.size() >= 2) {
                        x = pilha.pop();
                        // System.out.println("x = " + x + "");
                        y = pilha.pop();
                        // System.out.println("y = " + y + "");
                        result = x + y;
                        // System.out.println("result = " + result + "");
                        pilha.push(result);
                        //  System.out.println(pilha.top());
                    } else {
                        System.out.println("tamanho da pilha insuficiente para operacao :(");
                    }
                }

                if (linha.equals("*")) {
                    if (pilha.size() >= 2) {
                        x = pilha.pop();
                        y = pilha.pop();
                        result = x * y;
                        pilha.push(result);
                        // System.out.println("multiplicacao: "+pilha.top()+"");
                    } else {
                        System.out.println("tamanho da pilha insuficiente para operacao :(");
                    }
                }
                if (linha.equals("-")) {
                    if (pilha.size() >= 2) {
                        x = pilha.pop();
                        // System.out.println("x = "+x+"");
                        y = pilha.pop();
                        //System.out.println("y = "+y+"");
                        result = x - y;
                        //System.out.println("result = "+result+"");
                        pilha.push(result);
                        // System.out.println("subtracao: "+pilha.top()+"");
                    } else {
                        System.out.println("tamanho da pilha insuficiente para operacao :(");
                    }
                }
                if (linha.equals("/")) {
                    if (pilha.size() >= 2) {
                        x = pilha.pop();
                        y = pilha.pop();
                        result = x / y;
                        pilha.push(result);
                        // System.out.println("divisao: "+pilha.top()+"");
                    } else {
                        System.out.println("tamanho da pilha insuficiente para operacao :(");
                    }
                }
                if (linha.equals("dup")) {
                    pilha.dup();
                    // System.out.println("deu bret");
                }
                if (linha.equals("pop")) {
                    pilha.pop();
                    // System.out.println("deu bret");
                }
                if (linha.equals("swap")) {
                    if (pilha.size() >= 2) {
                        //System.out.println(pilha.top());
                        pilha.swap();
                        //System.out.println("dpois do swap: "+pilha.top()+"");
                        //System.out.println("deu bret");
                    } else {
                        System.out.println("tamanho da pilha insuficiente para operacao :(");
                    }
                }
                if (linha.equals("chs")) {
                    pilha.chs();
                    //System.out.println("deu bret");
                }
                if (linha.equals("sqrt")) {
                    pilha.sqrt();
                    //System.out.println("deu bret");
                }
            } else {

                num = Double.parseDouble(linha);

                pilha.push(num);
                // System.out.println("foi pra pilha: "+pilha.top()+"");
            }
            if (pilha.size() > tamMax) {
                tamMax = pilha.size();
            }
            System.out.println("Topo da pilha: " + pilha.top() + "");
            //System.out.println("tamanho da pilha: " + pilha.size() + "");
        }
        System.out.println("Tamanho maximo da pilha: " + getTamMax() + "");
        System.out.println("Resultado armazenado no topo da pilha: " + pilha.top() + "");
        fr.close();
        br.close();
    }

    public int getTamMax() {
        return tamMax;
    }


}
